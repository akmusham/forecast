import axios from 'axios'
import * as types from '../constants/city'

const SERVER_URL = `https://api.openweathermap.org/data/2.5`

export const citySuccess = (payload) => ({
  type: types.CITY_SUCCESS,
  ...payload
})

export const cityFail = () => ({
  type: types.CITY_FAIL
})

export const cityRequest = () => ({
  type: types.CITY_REQUEST
})

export const AddCity = (city) => {
  return async dispatch => {
    try {
      dispatch(cityRequest())
      let config = {
        method: 'get',
        baseURL: SERVER_URL,
      }
      let { data } = await axios.get(`${SERVER_URL}/weather?q=${city}&appid=c51223c219d6aec8cb8c5210449bd859`)
      let Forecast = await  axios.get(`${SERVER_URL}/forecast/daily?q=${city}&cnt=5&appid=c51223c219d6aec8cb8c5210449bd859`)
      data.Forecast = Forecast.data.list
      dispatch(citySuccess(data))

    } catch (e) {
      dispatch(cityFail())
    }
  }
}
