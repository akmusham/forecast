import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducer from './reducers';


import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'



const persistConfig = {
	key: 'root',
	storage
}

const persistedReducer = persistReducer(persistConfig, reducer)

const middleware = applyMiddleware(
	thunk,

);


const middleware_prod = applyMiddleware(
	thunk
);

console.log(process.env.NODE_ENV)

export let store = createStore(persistedReducer, middleware)
export let persistor = persistStore(store)
