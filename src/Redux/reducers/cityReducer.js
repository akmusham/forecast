import * as types from '../constants/city'

const initialState = {
  isFetching: false,
  cities: [],
  selectedCity: {}
}


const CityReducer = (state = initialState, action) => {

  switch (action.type) {

    case types.CITY_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      })

    case types.CITY_FAIL:
      return Object.assign({}, state, {
        isFetching: false
      })

    case types.CITY_SUCCESS:
      let newlist = state.cities
      // console.log(newlist);
      newlist.push(action)
      // state.id = this.state.RecentlyAddedLocations.length + 1
      // if (this.state.RecentlyAddedLocations.length > 7) {
      //     tempdata.splice(7,1)
      // }
      return Object.assign({}, state, {
        isFetching: false,
        cities: newlist,
        selectedCity:action
      })

    default:
      return state
  }
}

export default CityReducer
